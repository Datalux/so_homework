/*
    Homework n.1

    Scrivere un programma in linguaggio C che permetta di copiare un numero
    arbitrario di file regolari su una directory di destinazione preesistente.

    Il programma dovra' accettare una sintassi del tipo:
     $ homework-1 file1.txt path/file2.txt "nome con spazi.pdf" directory-destinazione
*/


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define BUFSIZE 4096

int main(int argc, char* argv[]){
  int sd, dd, size, result;
  char buffer[BUFSIZE];
  char *path, *path2;


  //controllo sul numero dei parametri
  if(argc < 3){
    printf("Utilizzo: %s <sorgente>[... <sorgente>] <destinazione>\n", argv[0]);
    exit(1);
  }


  for(int i = 1; i < argc-1; i++){

    printf("\n[+] Apertura di %s in corso...\n", argv[i]);
    //apre il primo file delle sorgenti in sola lettura
    if((sd = open(argv[i], O_RDONLY)) == -1){
      perror(argv[i]);
      exit(1);
    }

    //prepara il nome del file di destinazione
    strncpy(buffer,argv[argc-1],BUFSIZE);
    size = strlen(buffer);
    strncpy(buffer+size,"/",BUFSIZE-size);
    size++;

    path = path2 = argv[i];
    while(*path2 != '\0'){
      if(*path2 == '/') path = path2+1;
      path2++;
    }
    strncpy(buffer+size, path, BUFSIZE-size);

    printf("[+] Copia di %s in %s in corso...\n",argv[i],buffer);
    //apre il file nella directory di destinazione in sola scrittura con troncamento e creazione
    if((dd = open(buffer,O_WRONLY | O_CREAT | O_TRUNC, 0660)) == -1){
      perror(buffer);
      exit(1);
    }


    //copia i dati da sorgente a destinazione
    do{
      //legge fino ad un massimo d BUFSIZE byte dalla sorgente
      if((size = read(sd, buffer, BUFSIZE)) == -1){
        perror(argv[i]);
        exit(1);
      }
      //scrive i byte letti
      if((result = write(dd, buffer, size)) == -1){
        perror(argv[i]);
        exit(1);
      }
    }while(size == BUFSIZE);

    //chiude i file aperti
    close(sd);
    close(dd);
    printf("[+] Copia effettauata con successo\n");

    }

}
